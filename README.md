# Introduction
This is a test project for getting an understanding of an applicant's C++ skills. The project contains multiple issues that the applicant is expected to either fix, or make a comment of. It is not expected that all issues are fixed. Applicant is also expected to comment on code quality (style, tests, re-usability, etc.) and the overall project structure and compilation. The applicant may use or refer to 3rd party libraries and applications, as long as the decision to do so is well argued.

# The project
This project implements a command line application which accepts instructions to generate ASCII strings by modifying a given string. The application implements following commands:
* -h, --help
  * list available commands, and example of basic usage
* -c, --count
  * expects an integer value to follow
  * the number of random strings to generate
* -rc, --random-count
  * may have an integer value to follow
  * defaults to 4
  * random number of strings to generate in the range of [1, rc], e.g. [1, 32]
* -sl, --shift_left
  * generates new strings by rotating the characters in the given string
* -sr, --shift-right
  * generates new strings by rotating the characters in the given string
* -ra, --random-ascii
  * generates new strings by replacing a random number of characters with a random ASCII character

Only one of -c and -rc may be used, the generation commands -sl, -sr, -ra are applied in the order given to the application.