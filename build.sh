#!/bin/bash
rm -rf ./build
mkdir build
clang++ -I ./cmd -c -o ./build/cmd.o ./cmd/src/cmd.cpp
clang++ -I ./cmd -c -o ./build/cmd_parse.o ./cmd/src/cmd_parse.cpp
clang++ -I ./cmd -c -o ./build/cmd_print.o ./cmd/src/cmd_print.cpp
clang++ -I ./cmd -c -o ./build/main.o ./cmd/main.cpp
clang++ -o ./build/cmd ./build/cmd.o ./build/cmd_parse.o ./build/cmd_print.o ./build/main.o