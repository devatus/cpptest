#include "include/cmd.hpp"

auto main( char** argv, int argc ) -> int
{
    return execute_command( argv, argc );
}