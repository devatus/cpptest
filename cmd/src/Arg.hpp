#include <string>

struct Arg
{
    std::string arg;
    std::string short_arg;
};

inline auto operator==( const Arg& arg, const std::string& cmd_arg ) -> bool
{
    return cmd_arg == arg.arg || arg.short_arg == cmd_arg;
}

inline auto operator==( const std::string& cmd_arg, const Arg& arg ) -> bool
{
    return  arg == cmd_arg;
}

inline auto operator!=( const std::string& cmd_arg, const Arg& arg ) -> bool
{
    return !( arg == cmd_arg );
}