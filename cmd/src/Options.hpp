#include <vector>
#include <string>

enum GeneratorType
{
    ShiftLeft, ShiftRight, RandomASCII
};

struct Options
{
    std::vector<GeneratorType> generators;
    std::string string;
    int count;
    bool r_count;
};