#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include "cmd_parse.hpp"
#include "cmd_print.hpp"

const auto HELP_TEXT = std::string(
    "Generate new strings from a given string\n"
    "Example:\n"
    "   cmd -c 10 -sl \"String to modify\"\n"
    "Commands:\n"
    "-h, --help                 List available commands and show example usage\n"
    "-c, --count <integer>      The number of strings to generate\n"
    "-rc, --random-count [max]  Generate a random number of strings up to \"max\", defaults to 32\n"
    "Generator commands:\n"
    "-sl, --shift-left          Generates new strings by rotating the characters in the given string\n"
    "-sr, --shift-right         Generates new strings by rotating the characters in the given string\n"
    "-ra, --random-ascii        Generates new strings by replacing a random number of characters with a random ASCII character\n"
);

static void print_error( int err )
{
    std::cout << "Error [" << err << "]: ";
    switch( err )
    {
    case -1:
    case -2:
        std::cout << "missing count";
        break;
    case -3:
        std::cout << "missing a string to modify";
        break;
    default:
        break;
    }
}

auto execute_command( char** argv, int argc ) -> int
{
    auto args = std::vector<std::string>{};
    for( auto i = 1; i < argc; ++i )
        args.push_back( *( argv + i ) );

    if( args[0] == "--help" || args[0] == "-h" )
    {
        std::cout << HELP_TEXT;
        return 0;
    }

    auto options = parse_commands( args );
    if( options.first != 0 )
    {
        print_error( options.first );
        return options.first;
    }

    print_text( options.second );

    return 0;
}