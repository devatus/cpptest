#include "cmd_parse.hpp"
#include <algorithm>
#include "Arg.hpp"

static auto parse_counts( std::vector<std::string>& args, Options* out )
{
    const auto fn_parse_count_number = [&]( auto count_num_it )
    {
        if( count_num_it == args.end() )
            return std::make_pair( false, 0 );

        return std::make_pair(
            true,
            std::atoi( count_num_it->c_str() )
            );
    };

    auto it = args.begin();
    if( *it == Arg{ "--count", "-c" } )
    {
        const auto number = fn_parse_count_number( ++it );

        if( !number.first )
            return std::make_pair( -2, args.end() );

        out->count = number.second;

        if( out->count <= 0 )
            return std::make_pair( -2, args.end() );

        ++it;
    }
    else if( *it == Arg{ "--random-count", "-rc" } )
    {
        out->r_count = true;
        const auto number = fn_parse_count_number( ++it );

        if( !number.first )
            out->count = 4;
        else
        {
            out->count = number.second;
            if( out->count )
                ++it;
            else
                out->count = 4;
        }
    }
    else
        return std::make_pair( -1, args.end() );

    return std::make_pair( 0, it );
}

struct GeneratorArg : Arg
{
    GeneratorType type;

    GeneratorArg( std::string a, std::string sa, GeneratorType t )
        : Arg{ a, sa }, type(t) {}
};

static auto parse_commands( std::vector<std::string> args ) -> std::pair<int, Options>
{
    auto options = Options{};
    
    auto nextIt = parse_counts( args, &options );
    if( nextIt.first )
        return std::make_pair( nextIt.first, Options{} );

    auto it = nextIt.second;

    auto generatorArgs = std::vector<GeneratorArg>{};
    generatorArgs.emplace_back( "--shift-left", "-sl", GeneratorType::ShiftLeft );
    generatorArgs.emplace_back( "--shift-right", "-sr", GeneratorType::ShiftRight );

    for( ; it != args.end(); ++it )
    {
        const auto generator = std::find( generatorArgs.begin(), generatorArgs.end(), *it );
        if( generator == generatorArgs.end() )
            break;

        options.generators.push_back( generator->type );
    }

    if( it == args.end() )
        return std::make_pair( -3, Options{} );

    options.string = *it;

    return std::make_pair( 0, options );
}