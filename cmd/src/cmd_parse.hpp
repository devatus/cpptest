#include <vector>
#include <string>
#include "Options.hpp"

auto parse_commands( std::vector<std::string> args ) -> std::pair<int, Options>;