#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <ctime>
#include "cmd_print.hpp"

auto g_value = 0;

void set_random_value( int value )
{
    g_value = value;
}

static auto get_random_count( int max ) -> int
{
    auto value = g_value;
    if( !value )
    {
        srand( time( NULL ) );
        value = rand();
    }
    return (value % max) + 1;
}

class Generator
{
public:
    virtual auto Generate( std::string value, size_t index ) -> std::string = 0;
};

class LeftShift : public Generator
{
public:
    auto Generate( std::string value, size_t index ) -> std::string override
    {
        index = index % value.size();
        std::rotate( value.begin(), value.begin() + index, value.end() );
        return value;
    }
};

class RightShift : public Generator
{
public:
    auto Generate( std::string value, size_t index ) -> std::string override
    {
        index = index % value.size();
        std::rotate( value.rbegin(), value.rbegin() + index, value.rend() );
        return value;
    }
};

void print_text( Options opt )
{
    auto count = opt.count;
    if( opt.r_count )
        count = get_random_count( count );

    auto leftShift = LeftShift{};
    auto rightShift = RightShift{};

    auto generators = std::vector<Generator*>();
    for( auto gen : opt.generators )
    {
        switch( gen )
        {
        case GeneratorType::ShiftLeft:
            generators.push_back(&leftShift);
            break;
        case GeneratorType::ShiftRight:
            generators.push_back(&rightShift);
            break;
        default:
            break;
        }
    }

    for( auto i = 0; i < count - 1; ++i )
    {
        auto value = opt.string;
        for( auto gen : generators )
            value = gen->Generate( value, i );

        std::cout << value << "\n";
    }

    auto value = opt.string;
    for( auto gen : generators )
        value = gen->Generate( value, count - 1 );

    std::cout << value;
}