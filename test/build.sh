#!/bin/bash
rm -rf ./build
mkdir build
clang++ -I ../cmd -c -o ./build/cmd.o ../cmd/src/cmd.cpp
clang++ -I ../cmd -c -o ./build/cmd_parse.o ../cmd/src/cmd_parse.cpp
clang++ -I ../cmd -c -o ./build/cmd_print.o ../cmd/src/cmd_print.cpp
clang++ -I ../cmd -c -o ./build/cmd.tests.o test.cpp
clang++ -o ./build/cmd.tests ./build/cmd.o ./build/cmd_parse.o ./build/cmd_parse.o ./build/cmd.tests.o