#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include <string>
#include <iostream>
#include "include/cmd.hpp"

struct StdOutHelper
{
    std::stringstream out;
    std::streambuf* std_out;

    StdOutHelper()
    {
        std_out = std::cout.rdbuf();
        std::cout.rdbuf( out.rdbuf() );
    }

    ~StdOutHelper()
    {
        std::cout.rdbuf( std_out );
    }

    auto str() { return out.str(); };
};

TEST_CASE( "--help lists available commands", "[app]" )
{
    StdOutHelper out;

    SECTION( "full command" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--help" };
        (void)execute_command(cmds, 2);
    }
    SECTION( "short command" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"-h" };
        (void)execute_command(cmds, 2);
    }

    const auto help_text = std::string(
        "Generate new strings from a given string\n"
        "Example:\n"
        "   cmd -c 10 -sl \"String to modify\"\n"
        "Commands:\n"
        "-h, --help                 List available commands and show example usage\n"
        "-c, --count <integer>      The number of strings to generate\n"
        "-rc, --random-count [max]  Generate a random number of strings up to \"max\", defaults to 32\n"
        "Generator commands:\n"
        "-sl, --shift-left          Generates new strings by rotating the characters in the given string\n"
        "-sr, --shift-right         Generates new strings by rotating the characters in the given string\n"
        "-ra, --random-ascii        Generates new strings by replacing a random number of characters with a random ASCII character\n"
    );

    REQUIRE( out.str() == help_text );
}

TEST_CASE( "--count without generator commands", "[app]" )
{
    StdOutHelper out;

    SECTION( "with no text, reports error" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"1" };
        auto ret = execute_command(cmds, 3);

        REQUIRE( ret != 0 );
        REQUIRE( out.str() == "Error [-3]: missing a string to modify" );
    }
    SECTION( "with no count number, reports error" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"some nice string" };
        auto ret = execute_command(cmds, 3);

        REQUIRE( ret != 0 );
        REQUIRE( out.str() == "Error [-2]: missing count" );
    }
    SECTION( "prints given string count times" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"some nice string" };
        auto ret = execute_command(cmds, 4);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nsome nice string\nsome nice string" );
    }
    SECTION( "short command, prints given string count times" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"-c", (char*)"3", (char*)"some nice string" };
        auto ret = execute_command(cmds, 4);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nsome nice string\nsome nice string" );
    }
}

TEST_CASE( "--random-count without generator commands", "[app]" )
{
    StdOutHelper out;

    set_random_value( 3 );

    SECTION( "with no text, reports error" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--random-count" };
        auto ret = execute_command(cmds, 2);

        REQUIRE( ret != 0 );
        REQUIRE( out.str() == "Error [-3]: missing a string to modify" );
    }
    SECTION( "with no count number, uses default" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--random-count", (char*)"some nice string" };
        auto ret = execute_command(cmds, 3);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nsome nice string\nsome nice string\nsome nice string" );
    }
    SECTION( "prints given string a maximum of count times" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--random-count", (char*)"2", (char*)"some nice string" };
        auto ret = execute_command(cmds, 4);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nsome nice string" );
    }
    SECTION( "short command, prints given string a maximum of count times" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"-rc", (char*)"2", (char*)"some nice string" };
        auto ret = execute_command(cmds, 4);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nsome nice string" );
    }
}

TEST_CASE( "--shift-left", "[app]" )
{
    StdOutHelper out;

    SECTION( "with no count, reports error" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--shift-left", (char*)"some nice string" };
        auto ret = execute_command(cmds, 3);

        REQUIRE( ret != 0 );
    }
    SECTION( "with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"--shift-left", (char*)"some nice string" };
        auto ret = execute_command(cmds, 5);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nome nice strings\nme nice stringso" );
    }
    SECTION( "short command, with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"-sl", (char*)"some nice string" };
        auto ret = execute_command(cmds, 5);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nome nice strings\nme nice stringso" );
    }
    SECTION( "short command twice, with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"-sl", (char*)"-sl", (char*)"some nice string" };
        auto ret = execute_command(cmds, 6);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nme nice stringso\n nice stringsome" );
    }
}

TEST_CASE( "--shift-right", "[app]" )
{
    StdOutHelper out;

    SECTION( "with no count, reports error" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--shift-right", (char*)"some nice string" };
        auto ret = execute_command(cmds, 3);

        REQUIRE( ret != 0 );
    }
    SECTION( "with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"--shift-right", (char*)"some nice string" };
        auto ret = execute_command(cmds, 5);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\ngsome nice strin\nngsome nice stri" );
    }
    SECTION( "short command, with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"-sr", (char*)"some nice string" };
        auto ret = execute_command(cmds, 5);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\ngsome nice strin\nngsome nice stri" );
    }
    SECTION( "short command twice, with count, prints shifted strings" )
    {
        char* cmds[] = { (char*)"ignored", (char*)"--count", (char*)"3", (char*)"-sr", (char*)"-sr", (char*)"some nice string" };
        auto ret = execute_command(cmds, 6);

        REQUIRE( ret == 0 );
        REQUIRE( out.str() == "some nice string\nngsome nice stri\nringsome nice st" );
    }
}

